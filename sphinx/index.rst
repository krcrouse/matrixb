.. documentation master file

matrixb
========================================

An extensible package to automate and enhance the manipulation of tabular/matrix data without giving up performance.

.. toctree::
   :maxdepth: 3
   :caption: Contents:

Overview
========

This is the API documentation for the matrixb Python package. The source for the package along with a more user-friendly README with general use examples are located at the `GitLab project page <https://gitlab.com/krcrouse/matrixb>`_

Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
