matrixb package
===============

Subpackages
-----------

.. toctree::

    matrixb.source

Submodules
----------

matrixb.columnnormalizer module
-------------------------------

.. automodule:: matrixb.columnnormalizer
    :members:
    :undoc-members:
    :show-inheritance:

matrixb.iterator module
-----------------------

.. automodule:: matrixb.iterator
    :members:
    :undoc-members:
    :show-inheritance:

matrixb.matrix module
---------------------

.. automodule:: matrixb.matrix
    :members:
    :undoc-members:
    :show-inheritance:

matrixb.rowmap module
---------------------

.. automodule:: matrixb.rowmap
    :members:
    :undoc-members:
    :show-inheritance:


Module contents
---------------

.. automodule:: matrixb
    :members:
    :undoc-members:
    :show-inheritance:
