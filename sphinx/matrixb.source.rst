matrixb.source package
======================

Submodules
----------

matrixb.source.base module
--------------------------

.. automodule:: matrixb.source.base
    :members:
    :undoc-members:
    :show-inheritance:

matrixb.source.csv module
-------------------------

.. automodule:: matrixb.source.csv
    :members:
    :undoc-members:
    :show-inheritance:

matrixb.source.googlesheet module
---------------------------------

.. automodule:: matrixb.source.googlesheet
    :members:
    :undoc-members:
    :show-inheritance:

matrixb.source.resident module
------------------------------

.. automodule:: matrixb.source.resident
    :members:
    :undoc-members:
    :show-inheritance:

matrixb.source.xls module
-------------------------

.. automodule:: matrixb.source.xls
    :members:
    :undoc-members:
    :show-inheritance:

matrixb.source.xlsx module
--------------------------

.. automodule:: matrixb.source.xlsx
    :members:
    :undoc-members:
    :show-inheritance:


Module contents
---------------

.. automodule:: matrixb.source
    :members:
    :undoc-members:
    :show-inheritance:
