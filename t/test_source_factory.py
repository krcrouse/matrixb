import pytest
import os, sys, shutil
import datetime

sys.path.append('.')
import matrixb

def setup_module():
    pass

def array_matrix():
    return(
        [
            ['A','B','C','D','E'],
            [1,2,3,4,5],
            [6,7,8,9,10],
            [11,12,13,14,15],
        ]
    )


class TestResident():

    def test_source_factory(self):

        source = matrixb.source.factory(array_matrix())
        assert type(source) is matrixb.source.Resident
