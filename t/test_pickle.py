import pytest
import os, sys, shutil
import datetime
import pickle
import tempfile

sys.path.insert(0, '.')
import matrixb

def setup_module():
    pass


class BaseTest():
    data = [[1,2,3,4,5], [6,7,8,9,10], [11,12,13,14,15], [16,17,18,19,20]]
    columns = ['A','B','C','D','E']
    
    def setup_class(self):
        # we expect that the file_suffix property is set and export the matrix to a temporary file of that type. 
        mb = matrixb.matrix.Matrix(self.data, columns=self.columns)
        datafile = tempfile.NamedTemporaryFile(delete=False, suffix='.' + self.file_suffix)
        self.filename = datafile.name
        datafile.close()
        mb.export(self.filename)
        
        # now load the matrix from the file that we just exported to
        self.matrix = matrixb.Matrix(self.filename)

    def teardown_class(self):
        os.remove(self.filename)

    def test_output(self):
        source_mb = self.matrix
        file = tempfile.NamedTemporaryFile(delete=False)
        pickle.dump(source_mb, file)
        file.close()
        with open(file.name, 'rb') as fh:
            restored = pickle.load(fh)
        os.remove(file.name)

        for i, col in enumerate(self.columns):
            assert source_mb.columns[i] != col, "Original matrix column was normalized"
            assert restored.columns[i] != col, "Restored matrix column is the normalized value"

        #
        # This round of testing verifies that the data is there and can be referenced by index
        #
        for i, row in enumerate(self.data):
            for j, val in enumerate(row):
                assert source_mb[i][j] == val, f"Original matrix value is correct for ({i}, {j})"
                assert restored[i][j] == val, f"Restored matrix value is correct for ({i},{j})"
        assert source_mb.nrows == restored.nrows, "Nrows match"
        assert restored.nrows == len(self.data), "restored nrows matches data rows"
        assert source_mb.source.filename == restored.source.filename, "Filename is valid and matches"

        # this ensures that the data can be iterated
        itersource = iter(source_mb)
        nrows = 0
        for row in restored:
            sourcerow = next(itersource)
            icol = 0
            for cell in row:
                assert cell == sourcerow[icol], "values match"
                icol += 1
            assert icol == len(sourcerow), "lengths are the same"
            nrows += 1
        assert nrows == len(source_mb), "row count is the same"

        # this ensures that the rowmaps can be iterated
        itersource = iter(source_mb.rowmap())
        nrows = 0
        for row in restored.rowmap():
            sourcerow = next(itersource)
            icol = 0
            for key, val in row.items():
                assert val == sourcerow[key], "values match"
                icol += 1
            assert icol == len(sourcerow), "lengths are the same"
            nrows += 1
        assert nrows == len(source_mb), "row count is the same"


class TestPickleResidentMatrix(BaseTest):

    def setup_class(self):
        """ We don't export to the tempfile. Just set the matrix"""
        self.matrix = matrixb.Matrix(self.data, self.columns)

    def teardown_class(self):
        pass

class TestPickleCSV(BaseTest):
    file_suffix = 'csv'

class TestPickleXLSX(BaseTest):
    file_suffix = 'xlsx'

class TestPickleXLS(BaseTest):
    file_suffix = 'xls'

#class TestPickleODS(BaseTest):
#    file_suffix = 'ods'

#class TestPickleGoogleSheet(BaseTest):
#    file_suffix = 'xls'
