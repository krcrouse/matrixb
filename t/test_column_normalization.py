import pytest
import os, sys, shutil
import datetime

sys.path.insert(0, '../datacleaner')

sys.path.insert(0, '.')
import matrixb

def setup_module():
    pass

class TestColumnNaming():

    def test_lowercases(self):
        mb = matrixb.matrix.Matrix( [['', ' ', None],
                                     [' what', ' 1', '1.33 ']],
                                    columns=['A  ', ' B', 'C '])

        assert mb.columns[0] == 'a'
        assert mb.columns[1] == 'b'
        assert mb.columns[2] == 'c'
        assert mb.ncolumns == 3

    def test_column_naming_conditions(self):

        column_sets = [
            # simple, single letter variants
            [('A','B','C','D','E'), ['a','b','c','d','e']],
            [(' A','B ','  C','D   ',' E '), ['a','b','c','d','e']],
            [['_A','__B__',"C\n","\nD",'\nE  \n'], ['a','b','c','d','e']],
            # multiple letter and word variants
            [(' A and B','B   or c','  C  YES','D  OK O K OK O K      OK '," E\n AND \n\n\nF     G "),
                ['a_and_b','b_or_c','c_yes','d_ok_o_k_ok_o_k_ok','e_and_f_g']],
            [['_A','__B__',"C\n","\nD",'\nE  \n'], ['a','b','c','d','e']],
            # special characters, parenthesis, etc
            [('A (or b)','  B(x)','C^+',' @D footnote** ',"E!! Let\'s do this!!", 'e^{woof*2}', '1+2'),
            ['a_or_b','b_x','c','d_footnote','e_lets_do_this', 'e_woof_2', '1_2']],
            # allowed special characters
            [('count #', 'percent %', '#items', '%success', 'this/that'),
             ('count_#', 'percent_%', '#_items', '%_success', 'this_that')],
            # camel case
            [(  'camelCase',
                ' camelCase2   ',
                ' CamelCase     CamelCaseCase',
                'camelCase CamelCase CamelCaseCaseCase    camelCase camel_Case',
                'AYPCase',
                'MyLOVEPrevention SECLand',
                'love2brew internal533numbers2',
                '12monkeys',
            ),
             (  'camel_case',
                'camel_case_2',
                'camel_case_camel_case_case',
                'camel_case_camel_case_camel_case_case_case_camel_case_camel_case',
                'ayp_case',
                'my_love_prevention_sec_land',
                'love_2_brew_internal_533_numbers_2',
                '12_monkeys',
            )],
        ]
        mb = matrixb.Matrix()
        self.run_column_tests(mb, column_sets)

    def test_duplicate_names(self):
        column_sets = [
            # test column duplications
            [ ('a', 'a', 'a_', '  a', "\n       a      \n\n    "),
              ('a.1', 'a.2', 'a.3', 'a.4', 'a.5'),
             ],
            [ ('ax b_x', 'Ax b_X', 'ax B___x', '  AX     B_X ', "\n       ax      \n\n    B_X"),
              ('ax_b_x.1', 'ax_b_x.2', 'ax_b_x.3', 'ax_b_x.4', 'ax_b_x.5'),
             ],

            # test null columns
            [ [None], ['<blank>']],
            [  (None, None, None),
               ('<blank>.1','<blank>.2','<blank>.3')],
            [
               (None, '', ' ', "\n"),
               ('<blank>.1','<blank>.2','<blank>.3','<blank>.4'),
            ],
        ]
        mb = matrixb.Matrix()
        self.run_column_tests(mb, column_sets)


    def run_column_tests(self, origmb, column_sets):

        for pair in column_sets:
            mb = origmb.copy()
            #mb = matrixb.Matrix(columns=pair[0])
            mb.columns = pair[0]
            print("columns = ", mb.columns)
            for i in range(mb.ncolumns):
                assert mb.columns[i] == pair[1][i], "Expected Column " + str(i) + ", '"+pair[0][i]+"', to be normalized to '" + str(pair[1][i]) + "', but it was normalized to " + str(mb.columns[i]) + "\n\tAll Columns: " + str(mb.columns)
