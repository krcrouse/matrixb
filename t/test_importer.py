import pytest
import os, sys, shutil
import datetime

sys.path.append('.')
import matrixb

def setup_module():
    pass 
    
class TestImportFactory():

    def setup_class(self):
        pass
    
    def teardown_class(self):
        pass 

    def test_import_factory(self):
        fn = "t/input/test.csv"
        factory = matrixb.Importer(fn)
        assert(factory)
        assert factory.filename == fn
        assert factory.worksheet is None
        
        for row in factory:
            print("What what", row)
        assert factory
            
class TestMatrix():
    
    def test_simplematrix(self):
        mb = matrixb.matrix.Matrix([[1,2,3,4,5], [6,7,8,9,10], [11,12,13,14,15]], 
                                   columns=['A','B','C','D','E'])
        
        assert mb
        assert mb.columns[0] == 'a'
        assert mb.columns[4] == 'e'
        assert mb[0][0] == 1
        assert mb[2][2] == 13
        
        assert len(mb) == 3
        assert mb.ncolumns == 5
        assert len(mb) == mb.nrows

        irow = 0
        for row in mb:
            assert row[0] == mb[irow][0]
            assert row[4] == mb[irow][4]
            irow += 1
        
    def test_formats(self):
        mb = matrixb.matrix.Matrix( [['', ' ', None], 
                                     [' what', ' 1', '1.33 ']], 
                                    columns=['A  ', ' B', 'C '])
        
        assert mb.columns[0] == 'a'
        assert mb.columns[1] == 'b'
        assert mb.columns[2] == 'c'
        assert len(mb) == 1
            
        assert mb[0][0] == 'what'
        assert mb[0][1] == 1
        assert mb[0][2] == 1.33
        
    def test_dates(self):
        
        data = [[1, '2017-01-03'], [2, '2/1/2018'], [3, '11/15/2019']]
        
        def validate(matrix):
            assert type(matrix[0][1]) is datetime.date
            assert matrix[0][1] == datetime.date(2017,1,3)
            assert matrix[1][1] == datetime.date(2018,2,1)
            assert matrix[2][1] == datetime.date(2019,11,15)
            
        
        mb = matrixb.Matrix(data, columns=['#', 'date'],
                            column_types=[int,datetime.date], 
                            )
        validate(mb)


        mb = matrixb.Matrix(data, columns=['#', 'date'],
                            column_types={1:datetime.date}, 
                            )
        validate(mb)

        mb = matrixb.Matrix(data, columns=['#', 'date'],
                            column_types={'date':datetime.date}, 
                            )
        validate(mb)

        
    def test_multiple_iterators(self):
        
        mb = matrixb.matrix.Matrix([[1,2,3,4,5], [6,7,8,9,10], [11,12,13,14,15], [16,17,18,19,20]], 
                                   columns=['A','B','C','D','E'])
        
        iter1 = iter(mb)
        test1_1 = next(iter1)
        test1_2 = next(iter1) 
        
        iter2 = iter(mb)
        test2_1 = next(iter2)
        assert test1_1[0] == test2_1[0]
        assert test1_1[3] == test2_1[3]        
        assert test1_2[0] != test2_1[0]
        assert test1_1[0] == mb[0][0]
        assert test2_1[0] == mb[0][0]
        
        test2_2 = next(iter2)
        assert test1_1[0] != test2_2[0]
        assert test1_1[3] != test2_2[3]        
        assert test1_2[0] == test2_2[0]    
        assert test2_1[2] == mb[0][2]
        assert test1_1[0] == mb[0][0]
        
        
        
    def test_column_naming_conditions(self):
        
        column_sets = [
            # simple, single letter variants
            [('A','B','C','D','E'), ['a','b','c','d','e']],
            [(' A','B ','  C','D   ',' E '), ['a','b','c','d','e']],
            [['_A','__B__',"C\n","\nD",'\nE  \n'], ['a','b','c','d','e']],
            # multiple letter and word variants            
            [(' A and B','B   or c','  C  YES','D  OK O K OK O K      OK '," E\n AND \n\n\nF     G "), 
                ['a_and_b','b_or_c','c_yes','d_ok_o_k_ok_o_k_ok','e_and_f_g']],
            [['_A','__B__',"C\n","\nD",'\nE  \n'], ['a','b','c','d','e']],
            # special characters, parenthesis, etc
            [('A (or b)','  B(x)','C^+',' @D footnote** ',"E!! Let\'s do this!!", 'e^{woof*2}', '1+2'), 
            ['a_or_b','b_x','c','d_footnote','e_lets_do_this', 'e_woof_2', '1_2']],
            # allowed special characters
            [('count #', 'percent %', '#items', '%success', 'this/that'), 
             ('count_#', 'percent_%', '#_items', '%_success', 'this_that')],
            # camel case
            [(  'camelCase', 
                ' camelCase2   ',
                ' CamelCase     CamelCaseCase',
                'camelCase CamelCase CamelCaseCaseCase    camelCase camel_Case',
                'AYPCase',
                'MyLOVEPrevention SECLand',
                'love2brew internal533numbers2',
                '12monkeys',
            ),
             (  'camel_case',
                'camel_case_2',
                'camel_case_camel_case_case', 
                'camel_case_camel_case_camel_case_case_case_camel_case_camel_case',
                'ayp_case',
                'my_love_prevention_sec_land',
                'love_2_brew_internal_533_numbers_2',
                '12_monkeys',
            )],
            # test column duplications
            [ ('a', 'a', 'a_', '  a', "\n       a      \n\n    "),
              ('a.1', 'a.2', 'a.3', 'a.4', 'a.5'),
             ],
            [ ('ax b_x', 'Ax b_X', 'ax B___x', '  AX     B_X ', "\n       ax      \n\n    B_X"),
              ('ax_b_x.1', 'ax_b_x.2', 'ax_b_x.3', 'ax_b_x.4', 'ax_b_x.5'),
             ],
            
            # test null columns
            [ [None], ['<blank>']],
            [  (None, None, None), 
               ('<blank>.1','<blank>.2','<blank>.3')],

            ]
            
            
        for pair in column_sets:
            mb = matrixb.Matrix(columns=pair[0])
            print("columns = ", mb.columns)
            for i in range(mb.ncolumns):                
                assert mb.columns[i] == pair[1][i]
        
        
                
        # test other variants of null - this won't work until clean is fixed
        v2 = [
            [  (None, '', ' ', "\n"), 
           ('<blank>.1','<blank>.2','<blank>.3','<blank>.4')],
        ]

        for pair in v2:
            mb = matrixb.Matrix(columns=pair[0])
            print("columns = ", mb.columns)
            for i in range(mb.ncolumns):                
                assert mb.columns[i] == pair[1][i]


        
        
