import pytest
#from delayed_assert import expect, assert_expectations

import os, sys, shutil
import datetime
import pdb,traceback,warnings

sys.path.append('.')
import matrixb


#['Int','Negative Int','Date - 2 Digit Year','Date - 4 Digit Year','ISO Date','Full Month Date','Abbreviated Date','Decimal','Float','Float 2'],

COLUMN_NAMES = ['Int','Negative Int','2 Digit Year','4 Digit Year','ISO Date','Full Month Date','Abbreviated Date','Decimal','Float','Float 2']
DATE_COLUMNS = ['2 Digit Year','4 Digit Year','ISO Date','Full Month Date','Abbreviated Date']

INLINE = [
    ['0','0','05/28/99','05/28/1999','1999-05-28','May 28, 1999','May 28, 1999','0.1','1.1','13935.1'],
    ['1','-1','06/04/99','06/04/1999','1999-06-04','June 4, 1999','Jun 4, 1999','0.01','10.01','8401.01'],
    ['2','-2','06/11/99','06/11/1999','1999-06-11','June 11, 1999','Jun 11, 1999','0.001','494.001','300.001'],
    ['7','-7','06/18/99','06/18/1999','1999-06-18','June 18, 1999','Jun 18, 1999','0.0001','9999.0001','11.0001'],
    ['9','-9','06/25/99','06/25/1999','1999-06-25','June 25, 1999','Jun 25, 1999','1E-05','83500.00001','9.00001'],
    ['10','-10','07/02/99','07/02/1999','1999-07-02','July 2, 1999','Jul 2, 1999','0.3','1.3','13935.3'],
    ['11','-11','07/09/99','07/09/1999','1999-07-09','July 9, 1999','Jul 9, 1999','0.04','10.04','8401.04'],
    ['12','-12','07/16/99','07/16/1999','1999-07-16','July 16, 1999','Jul. 16, 1999','0.006','494.006','300.006'],
    ['17','-17','07/23/99','07/23/1999','1999-07-23','July 23, 1999','Jul. 23, 1999','0.0008','9999.0008','11.0008'],
    ['19','-19','07/30/99','07/30/1999','1999-07-30','July 30, 1999','Jul. 30, 1999','9E-05','83500.00009','9.00009'],
    ['20','-20','08/06/99','08/06/1999','1999-08-06','August 6, 1999','Aug. 6, 1999','0.22','1.22','13935.22'],
    ['9.8E1','-9.8e1','08/13/99','08/13/1999','1999-08-13','August 13, 1999','Aug 13, 1999','0.033','10.033','8401.033'],
    ['99','-99','08/20/99','08/20/1999','1999-08-20','August 20, 1999','Aug. 20, 1999','0.0044','494.0044','300.0044'],
    ['100','-100','08/27/99','08/27/1999','1999-08-27','August 27, 1999','Aug. 27, 1999','0.00055','9999.00055','11.00055'],
    ['101','-101','09/03/99','09/03/1999','1999-09-03','September 3, 1999','Sep 3, 1999','7.7E-05','83500.000077','9.000077'],
    ['102','-102','09/10/99','09/10/1999','1999-09-10','September 10, 1999','Sep 10, 1999','0.784','1.784','13935.784'],
    ['.467e3','-.467E3','09/17/99','09/17/1999','1999-09-17','September 17, 1999','Sep 17, 1999','0.0472','10.0472','8401.0472'],
    ['499','-499','09/24/99','09/24/1999','1999-09-24','September 24, 1999','Sep 24, 1999','0.00908','494.00908','300.00908'],
    ['500','-500','10/01/99','10/01/1999','1999-10-01','October 1, 1999','Oct 1, 1999','0.000371','9999.000371','11.000371'],
    ['998','-998','10/08/99','10/08/1999','1999-10-08','October 8, 1999','Oct 8, 1999','3.21E-05','83500.0000321','9.0000321'],
    ['999','-999','10/15/99','10/15/1999','1999-10-15','October 15, 1999','Oct 15, 1999','0.8234','1.8234','13935.8234'],
    ['1000','-1000','10/22/99','10/22/1999','1999-10-22','October 22, 1999','Oct 22, 1999','0.01111','10.01111','8401.01111'],
    ['1001','-1001','10/29/99','10/29/1999','1999-10-29','October 29, 1999','Oct 29, 1999','0.004741','494.004741','300.004741'],
    ['1002','-1002','11/05/99','11/05/1999','1999-11-05','November 5, 1999','Nov 5, 1999','0.0007521','9999.0007521','11.0007521'],
    ['1099','-1099','11/12/99','11/12/1999','1999-11-12','November 12, 1999','Nov 12, 1999','8.267E-05','83500.00008267','9.00008267'],
    ['1100','-1100','11/19/99','11/19/1999','1999-11-19','November 19, 1999','Nov 19, 1999','0.99999','1.99999','13935.99999'],
    ['1111','-1111','11/26/99','11/26/1999','1999-11-26','November 26, 1999','Nov 26, 1999','0.023845','10.023845','8401.023845'],
    ['4722','-4722','12/03/99','12/03/1999','1999-12-03','December 3, 1999','Dec 3, 1999','0.0088245','494.0088245','300.0088245'],
    ['7833','-7833','12/10/99','12/10/1999','1999-12-10','December 10, 1999','Dec 10, 1999','0.00028853','9999.00028853','11.00028853'],
    ['9998','-9998','12/17/99','12/17/1999','1999-12-17','December 17, 1999','Dec 17, 1999','5.8319E-05','83500.000058319','9.000058319'],
    ['9999','-9999','12/24/99','12/24/1999','1999-12-24','December 24, 1999','Dec 24, 1999','-0.1','-1.1','-13935.1'],
    ['10000','-10000','12/31/99','12/31/1999','1999-12-31','December 31, 1999','Dec 31, 1999','-0.01','-10.01','-8401.01'],
    ['10001','-10001','01/07/00','01/07/2000','2000-01-07','January 7, 2000','Jan 7, 2000','-0.001','-494.001','-300.001'],
    ['27733','-27733','01/14/00','01/14/2000','2000-01-14','January 14, 2000','Jan 14, 2000','-0.0001','-9999.0001','-11.0001'],
    ['56868','-56868','01/21/00','01/21/2000','2000-01-21','January 21, 2000','Jan 21, 2000','-1E-05','-83500.00001','-9.00001'],
    ['83399','-83399','01/28/00','01/28/2000','2000-01-28','January 28, 2000','Jan 28, 2000','-0.3','-1.3','-13935.3'],
    ['99998','-99998','02/04/00','02/04/2000','2000-02-04','February 4, 2000','Feb 4, 2000','-0.04','-10.04','-8401.04'],
    ['99999','-99999','02/11/00','02/11/2000','2000-02-11','February 11, 2000','Feb 11, 2000','-0.006','-494.006','-300.006'],
    ['100000','-100000','02/18/00','02/18/2000','2000-02-18','February 18, 2000','Feb 18, 2000','-0.0008','-9999.0008','-11.0008'],
    ['100001','-100001','02/25/00','02/25/2000','2000-02-25','February 25, 2000','Feb 25, 2000','-9E-05','-83500.00009','-9.00009'],
    ['484839','-484839','03/03/00','03/03/2000','2000-03-03','March 3, 2000','Mar 3, 2000','-0.22','-1.22','-13935.22'],
    ['585885','-585885','03/10/00','03/10/2000','2000-03-10','March 10, 2000','Mar. 10, 2000','-0.033','-10.033','-8401.033'],
    ['999998','-999998','03/17/00','03/17/2000','2000-03-17','March 17, 2000','Mar. 17, 2000','-0.0044','-494.0044','-300.0044'],
    ['999999','-999999','03/24/00','03/24/2000','2000-03-24','March 24, 2000','Mar. 24, 2000','-0.00055','-9999.00055','-11.00055'],
    ['1e6','-1E6','03/31/00','03/31/2000','2000-03-31','March 31, 2000','Mar 31, 2000','-7.7E-05','-83500.000077','-9.000077'],
    ['1000001','-1000001','04/07/00','04/07/2000','2000-04-07','April 7, 2000','Apr 7, 2000','-0.784','-1.784','-13935.784'],
    ['1000002','-1000002','04/14/00','04/14/2000','2000-04-14','April 14, 2000','Apr 14, 2000','-0.0472','-10.0472','-8401.0472'],
    ['1883348','-1883348','04/21/00','04/21/2000','2000-04-21','April 21, 2000','Apr 21, 2000','-0.00908','-494.00908','-300.00908'],
    ['3845884','-3845884','04/28/00','04/28/2000','2000-04-28','April 28, 2000','Apr 28, 2000','-0.000371','-9999.000371','-11.000371'],
    ['5995968','-5995968','05/05/00','05/05/2000','2000-05-05','May 5, 2000','May 5, 2000','-3.21E-05','-83500.0000321','-9.0000321'],
    ['9999998','-9999998','05/12/00','05/12/2000','2000-05-12','May 12, 2000','May 12, 2000','-0.8234','-1.8234','-13935.8234'],
    ['9999999','-9999999','05/19/00','05/19/2000','2000-05-19','May 19, 2000','May 19, 2000','-0.01111','-10.01111','-8401.01111'],
    ['1000000','-1000000','05/26/00','05/26/2000','2000-05-26','May 26, 2000','May 26, 2000','-0.004741','-494.004741','-300.004741'],
    ['1000001','-1000001','06/02/00','06/02/2000','2000-06-02','June 2, 2000','Jun 2, 2000','-0.0007521','-9999.0007521','-11.0007521'],
    ['1000002','-1000002','06/09/00','06/09/2000','2000-06-09','June 9, 2000','Jun 9, 2000','-8.267E-05','-83500.00008267','-9.00008267'],
    ['3833894','-3833894','06/16/00','06/16/2000','2000-06-16','June 16, 2000','Jun 16, 2000','-0.99999','-1.99999','-13935.99999'],
    ['5958477','-5958477','06/23/00','06/23/2000','2000-06-23','June 23, 2000','Jun 23, 2000','-0.023845','-10.023845','-8401.023845'],
    ['8449938','-8449938','06/30/00','06/30/2000','2000-06-30','June 30, 2000','Jun 30, 2000','-0.0088245','-494.0088245','-300.0088245'],
    ['9999998','-9999998','07/07/00','07/07/2000','2000-07-07','July 7, 2000','Jul 7, 2000','-0.00028853','-9999.00028853','-11.00028853'],
    ['9999999','-9999999','07/14/00','07/14/2000','2000-07-14','July 14, 2000','Jul 14, 2000','-5.8319E-05','-83500.000058319','-9.000058319'],
    ['1.0e7','-10000000','7/21/00','7/21/2000','2000-07-21','July 21, 2000','Jul 21, 2000','.1','-.1', '07871'],
    ['10000001','-10000001','7/28/00','7/28/2000','2000-07-28','July 28, 2000','Jul 28, 2000','.01','-.01', '0011223344'],
    ['10000002','-10000002','8/04/00','8/04/2000','2000-08-04','August 4, 2000','Aug 4, 2000','.001','-.001', '00001'],
    ['34488499','-34488499','8/11/00','8/11/2000','2000-08-11','August 11, 2000','Aug 11, 2000','.0001','-.0001', '00'],
    ['58866869','-58866869','08/18/00','08/18/2000','2000-08-18','August 18, 2000','Aug 18, 2000','.00001','-.00001'],
    ['84499484','-84499484','08/25/00','08/25/2000','2000-08-25','August 25, 2000','Aug 25, 2000','.3','-.3'],
    ['99999998','-99999998','09/1/00','09/1/2000','2000-09-01','September 1, 2000','Sep 1, 2000','.04','-.04'],
    ['99999999','-99999999','09/8/00','09/8/2000','2000-09-08','September 8, 2000','Sep 8, 2000','.006','-.006'],
    ['100000000','-100000000','09/15/00','09/15/2000','2000-09-15','September 15, 2000','Sep 15, 2000','.0008','-.0008'],
    ['100000001','-100000001','09/22/00','09/22/2000','2000-09-22','September 22, 2000','Sep 22, 2000','.00009','-.00009'],
    ['100000002','-100000002','09/29/00','09/29/2000','2000-09-29','September 29, 2000','Sep 29, 2000','.22','-.22'],
    ['374599483','-374599483','10/6/00','10/6/2000','2000-10-06','October 6, 2000','Oct 6, 2000','.033','-.033'],
    ['595598587','-595598587','10/13/00','10/13/2000','2000-10-13','October 13, 2000','Oct 13, 2000','.0044','-.0044'],
    ['999999998','-999999998','10/20/00','10/20/2000','2000-10-20','October 20, 2000','Oct 20, 2000','.00055','-.00055'],
    ['999999999','-999999999','10/27/00','10/27/2000','2000-10-27','October 27, 2000','Oct 27, 2000','.000077','-.000077'],
    ['1000000000','-1.0E9','11/03/00','11/03/2000','2000-11-03','November 3, 2000','Nov 3, 2000','.784','-.784'],
    ['1000000001','-1000000001','11/10/00','11/10/2000','2000-11-10','November 10, 2000','Nov. 10, 2000','.0472','-.0472'],
    ['1000000002','-1000000002','11/17/00','11/17/2000','2000-11-17','November 17, 2000','Nov. 17, 2000','.00908','-.00908'],
    ['3747493856','-3747493856','11/24/00','11/24/2000','2000-11-24','November 24, 2000','Nov. 24, 2000','.000371','-.000371'],
    ['7869595843','-7869595843','12/01/00','12/01/2000','2000-12-01','December 01, 2000','Dec. 01, 2000','.0000321','-.0000321'],
    ['9999999998','-9999999998','12/08/00','12/08/2000','2000-12-08','December 08, 2000','Dec. 08, 2000','.8234','-.8234'],
    ['9999999999','-9999999999','12/15/00','12/15/2000','2000-12-15','December 15, 2000','Dec. 15, 2000','.01111','-.01111'],
    ['1.00e10','-10000000000','12/22/00','12/22/2000','2000-12-22','December 22, 2000','Dec. 22, 2000','.004741','-.004741'],
    ['10000000001','-10000000001','12/29/00','12/29/2000','2000-12-29','December 29, 2000','Dec. 29, 2000','.0007521','-.0007521'],
    ['27500485848','-27500485848','1/5/01','1/5/2001','2001-01-05','January 05, 2001','Jan. 05, 2001','.00008267','-.00008267'],
    ['65598383845','-65598383845','1/12/01','1/12/2001','2001-01-12','January 12, 2001','Jan. 12, 2001','.99999','-.99999'],
    ['99999999998','-99999999998','1/19/01','1/19/2001','2001-01-19','January 19, 2001','Jan. 19, 2001','.023845','-.023845'],
    ['99999999999','-99999999999','1/26/01','1/26/2001','2001-01-26','January 26, 2001','Jan. 26, 2001','.0088245','-.0088245'],
    ['100000000000','-100000000000','2/2/01','2/2/2001','2001-02-02','February 02, 2001','Feb. 02, 2001','.00028853','-.00028853'],
    ['100000000001','-100000000001','2/9/01','2/9/2001','2001-02-09','February 09, 2001','Feb. 09, 2001','.000058319','-.000058319'],
]

REFERENCE = [
    [0,0,datetime.date(1999,5,28),datetime.date(1999,5,28),datetime.date(1999,5,28),datetime.date(1999,5,28),datetime.date(1999,5,28),.1,1.1,13935.1],
    [1,-1,datetime.date(1999,6,4),datetime.date(1999,6,4),datetime.date(1999,6,4),datetime.date(1999,6,4),datetime.date(1999,6,4),.01,10.01,8401.01],
    [2,-2,datetime.date(1999,6,11),datetime.date(1999,6,11),datetime.date(1999,6,11),datetime.date(1999,6,11),datetime.date(1999,6,11),.001,494.001,300.001],
    [7,-7,datetime.date(1999,6,18),datetime.date(1999,6,18),datetime.date(1999,6,18),datetime.date(1999,6,18),datetime.date(1999,6,18),.0001,9999.0001,11.0001],
    [9,-9,datetime.date(1999,6,25),datetime.date(1999,6,25),datetime.date(1999,6,25),datetime.date(1999,6,25),datetime.date(1999,6,25),.00001,83500.00001,9.00001],
    [10,-10,datetime.date(1999,7,2),datetime.date(1999,7,2),datetime.date(1999,7,2),datetime.date(1999,7,2),datetime.date(1999,7,2),.3,1.3,13935.3],
    [11,-11,datetime.date(1999,7,9),datetime.date(1999,7,9),datetime.date(1999,7,9),datetime.date(1999,7,9),datetime.date(1999,7,9),.04,10.04,8401.04],
    [12,-12,datetime.date(1999,7,16),datetime.date(1999,7,16),datetime.date(1999,7,16),datetime.date(1999,7,16),datetime.date(1999,7,16),.006,494.006,300.006],
    [17,-17,datetime.date(1999,7,23),datetime.date(1999,7,23),datetime.date(1999,7,23),datetime.date(1999,7,23),datetime.date(1999,7,23),.0008,9999.0008,11.0008],
    [19,-19,datetime.date(1999,7,30),datetime.date(1999,7,30),datetime.date(1999,7,30),datetime.date(1999,7,30),datetime.date(1999,7,30),.00009,83500.00009,9.00009],
    [20,-20,datetime.date(1999,8,6),datetime.date(1999,8,6),datetime.date(1999,8,6),datetime.date(1999,8,6),datetime.date(1999,8,6),.22,1.22,13935.22],
    [98,-98,datetime.date(1999,8,13),datetime.date(1999,8,13),datetime.date(1999,8,13),datetime.date(1999,8,13),datetime.date(1999,8,13),.033,10.033,8401.033],
    [99,-99,datetime.date(1999,8,20),datetime.date(1999,8,20),datetime.date(1999,8,20),datetime.date(1999,8,20),datetime.date(1999,8,20),.0044,494.0044,300.0044],
    [100,-100,datetime.date(1999,8,27),datetime.date(1999,8,27),datetime.date(1999,8,27),datetime.date(1999,8,27),datetime.date(1999,8,27),.00055,9999.00055,11.00055],
    [101,-101,datetime.date(1999,9,3),datetime.date(1999,9,3),datetime.date(1999,9,3),datetime.date(1999,9,3),datetime.date(1999,9,3),.000077,83500.000077,9.000077],
    [102,-102,datetime.date(1999,9,10),datetime.date(1999,9,10),datetime.date(1999,9,10),datetime.date(1999,9,10),datetime.date(1999,9,10),.784,1.784,13935.784],
    [467,-467,datetime.date(1999,9,17),datetime.date(1999,9,17),datetime.date(1999,9,17),datetime.date(1999,9,17),datetime.date(1999,9,17),.0472,10.0472,8401.0472],
    [499,-499,datetime.date(1999,9,24),datetime.date(1999,9,24),datetime.date(1999,9,24),datetime.date(1999,9,24),datetime.date(1999,9,24),.00908,494.00908,300.00908],
    [500,-500,datetime.date(1999,10,1),datetime.date(1999,10,1),datetime.date(1999,10,1),datetime.date(1999,10,1),datetime.date(1999,10,1),.000371,9999.000371,11.000371],
    [998,-998,datetime.date(1999,10,8),datetime.date(1999,10,8),datetime.date(1999,10,8),datetime.date(1999,10,8),datetime.date(1999,10,8),.0000321,83500.0000321,9.0000321],
    [999,-999,datetime.date(1999,10,15),datetime.date(1999,10,15),datetime.date(1999,10,15),datetime.date(1999,10,15),datetime.date(1999,10,15),.8234,1.8234,13935.8234],
    [1000,-1000,datetime.date(1999,10,22),datetime.date(1999,10,22),datetime.date(1999,10,22),datetime.date(1999,10,22),datetime.date(1999,10,22),.01111,10.01111,8401.01111],
    [1001,-1001,datetime.date(1999,10,29),datetime.date(1999,10,29),datetime.date(1999,10,29),datetime.date(1999,10,29),datetime.date(1999,10,29),.004741,494.004741,300.004741],
    [1002,-1002,datetime.date(1999,11,5),datetime.date(1999,11,5),datetime.date(1999,11,5),datetime.date(1999,11,5),datetime.date(1999,11,5),.0007521,9999.0007521,11.0007521],
    [1099,-1099,datetime.date(1999,11,12),datetime.date(1999,11,12),datetime.date(1999,11,12),datetime.date(1999,11,12),datetime.date(1999,11,12),.00008267,83500.00008267,9.00008267],
    [1100,-1100,datetime.date(1999,11,19),datetime.date(1999,11,19),datetime.date(1999,11,19),datetime.date(1999,11,19),datetime.date(1999,11,19),.99999,1.99999,13935.99999],
    [1111,-1111,datetime.date(1999,11,26),datetime.date(1999,11,26),datetime.date(1999,11,26),datetime.date(1999,11,26),datetime.date(1999,11,26),.023845,10.023845,8401.023845],
    [4722,-4722,datetime.date(1999,12,3),datetime.date(1999,12,3),datetime.date(1999,12,3),datetime.date(1999,12,3),datetime.date(1999,12,3),.0088245,494.0088245,300.0088245],
    [7833,-7833,datetime.date(1999,12,10),datetime.date(1999,12,10),datetime.date(1999,12,10),datetime.date(1999,12,10),datetime.date(1999,12,10),.00028853,9999.00028853,11.00028853],
    [9998,-9998,datetime.date(1999,12,17),datetime.date(1999,12,17),datetime.date(1999,12,17),datetime.date(1999,12,17),datetime.date(1999,12,17),.000058319,83500.000058319,9.000058319],
    [9999,-9999,datetime.date(1999,12,24),datetime.date(1999,12,24),datetime.date(1999,12,24),datetime.date(1999,12,24),datetime.date(1999,12,24),-0.1,-1.1,-13935.1],
    [10000,-10000,datetime.date(1999,12,31),datetime.date(1999,12,31),datetime.date(1999,12,31),datetime.date(1999,12,31),datetime.date(1999,12,31),-0.01,-10.01,-8401.01],
    [10001,-10001,datetime.date(2000,1,7),datetime.date(2000,1,7),datetime.date(2000,1,7),datetime.date(2000,1,7),datetime.date(2000,1,7),-0.001,-494.001,-300.001],
    [27733,-27733,datetime.date(2000,1,14),datetime.date(2000,1,14),datetime.date(2000,1,14),datetime.date(2000,1,14),datetime.date(2000,1,14),-0.0001,-9999.0001,-11.0001],
    [56868,-56868,datetime.date(2000,1,21),datetime.date(2000,1,21),datetime.date(2000,1,21),datetime.date(2000,1,21),datetime.date(2000,1,21),-0.00001,-83500.00001,-9.00001],
    [83399,-83399,datetime.date(2000,1,28),datetime.date(2000,1,28),datetime.date(2000,1,28),datetime.date(2000,1,28),datetime.date(2000,1,28),-0.3,-1.3,-13935.3],
    [99998,-99998,datetime.date(2000,2,4),datetime.date(2000,2,4),datetime.date(2000,2,4),datetime.date(2000,2,4),datetime.date(2000,2,4),-0.04,-10.04,-8401.04],
    [99999,-99999,datetime.date(2000,2,11),datetime.date(2000,2,11),datetime.date(2000,2,11),datetime.date(2000,2,11),datetime.date(2000,2,11),-0.006,-494.006,-300.006],
    [100000,-100000,datetime.date(2000,2,18),datetime.date(2000,2,18),datetime.date(2000,2,18),datetime.date(2000,2,18),datetime.date(2000,2,18),-0.0008,-9999.0008,-11.0008],
    [100001,-100001,datetime.date(2000,2,25),datetime.date(2000,2,25),datetime.date(2000,2,25),datetime.date(2000,2,25),datetime.date(2000,2,25),-0.00009,-83500.00009,-9.00009],
    [484839,-484839,datetime.date(2000,3,3),datetime.date(2000,3,3),datetime.date(2000,3,3),datetime.date(2000,3,3),datetime.date(2000,3,3),-0.22,-1.22,-13935.22],
    [585885,-585885,datetime.date(2000,3,10),datetime.date(2000,3,10),datetime.date(2000,3,10),datetime.date(2000,3,10),datetime.date(2000,3,10),-0.033,-10.033,-8401.033],
    [999998,-999998,datetime.date(2000,3,17),datetime.date(2000,3,17),datetime.date(2000,3,17),datetime.date(2000,3,17),datetime.date(2000,3,17),-0.0044,-494.0044,-300.0044],
    [999999,-999999,datetime.date(2000,3,24),datetime.date(2000,3,24),datetime.date(2000,3,24),datetime.date(2000,3,24),datetime.date(2000,3,24),-0.00055,-9999.00055,-11.00055],
    [1000000,-1000000,datetime.date(2000,3,31),datetime.date(2000,3,31),datetime.date(2000,3,31),datetime.date(2000,3,31),datetime.date(2000,3,31),-0.000077,-83500.000077,-9.000077],
    [1000001,-1000001,datetime.date(2000,4,7),datetime.date(2000,4,7),datetime.date(2000,4,7),datetime.date(2000,4,7),datetime.date(2000,4,7),-0.784,-1.784,-13935.784],
    [1000002,-1000002,datetime.date(2000,4,14),datetime.date(2000,4,14),datetime.date(2000,4,14),datetime.date(2000,4,14),datetime.date(2000,4,14),-0.0472,-10.0472,-8401.0472],
    [1883348,-1883348,datetime.date(2000,4,21),datetime.date(2000,4,21),datetime.date(2000,4,21),datetime.date(2000,4,21),datetime.date(2000,4,21),-0.00908,-494.00908,-300.00908],
    [3845884,-3845884,datetime.date(2000,4,28),datetime.date(2000,4,28),datetime.date(2000,4,28),datetime.date(2000,4,28),datetime.date(2000,4,28),-0.000371,-9999.000371,-11.000371],
    [5995968,-5995968,datetime.date(2000,5,5),datetime.date(2000,5,5),datetime.date(2000,5,5),datetime.date(2000,5,5),datetime.date(2000,5,5),-0.0000321,-83500.0000321,-9.0000321],
    [9999998,-9999998,datetime.date(2000,5,12),datetime.date(2000,5,12),datetime.date(2000,5,12),datetime.date(2000,5,12),datetime.date(2000,5,12),-0.8234,-1.8234,-13935.8234],
    [9999999,-9999999,datetime.date(2000,5,19),datetime.date(2000,5,19),datetime.date(2000,5,19),datetime.date(2000,5,19),datetime.date(2000,5,19),-0.01111,-10.01111,-8401.01111],
    [1000000,-1000000,datetime.date(2000,5,26),datetime.date(2000,5,26),datetime.date(2000,5,26),datetime.date(2000,5,26),datetime.date(2000,5,26),-0.004741,-494.004741,-300.004741],
    [1000001,-1000001,datetime.date(2000,6,2),datetime.date(2000,6,2),datetime.date(2000,6,2),datetime.date(2000,6,2),datetime.date(2000,6,2),-0.0007521,-9999.0007521,-11.0007521],
    [1000002,-1000002,datetime.date(2000,6,9),datetime.date(2000,6,9),datetime.date(2000,6,9),datetime.date(2000,6,9),datetime.date(2000,6,9),-0.00008267,-83500.00008267,-9.00008267],
    [3833894,-3833894,datetime.date(2000,6,16),datetime.date(2000,6,16),datetime.date(2000,6,16),datetime.date(2000,6,16),datetime.date(2000,6,16),-0.99999,-1.99999,-13935.99999],
    [5958477,-5958477,datetime.date(2000,6,23),datetime.date(2000,6,23),datetime.date(2000,6,23),datetime.date(2000,6,23),datetime.date(2000,6,23),-0.023845,-10.023845,-8401.023845],
    [8449938,-8449938,datetime.date(2000,6,30),datetime.date(2000,6,30),datetime.date(2000,6,30),datetime.date(2000,6,30),datetime.date(2000,6,30),-0.0088245,-494.0088245,-300.0088245],
    [9999998,-9999998,datetime.date(2000,7,7),datetime.date(2000,7,7),datetime.date(2000,7,7),datetime.date(2000,7,7),datetime.date(2000,7,7),-0.00028853,-9999.00028853,-11.00028853],
    [9999999,-9999999,datetime.date(2000,7,14),datetime.date(2000,7,14),datetime.date(2000,7,14),datetime.date(2000,7,14),datetime.date(2000,7,14),-0.000058319,-83500.000058319,-9.000058319],
    [10000000,-10000000,datetime.date(2000,7,21),datetime.date(2000,7,21),datetime.date(2000,7,21),datetime.date(2000,7,21),datetime.date(2000,7,21),.1,-.1,'07871',],
    [10000001,-10000001,datetime.date(2000,7,28),datetime.date(2000,7,28),datetime.date(2000,7,28),datetime.date(2000,7,28),datetime.date(2000,7,28),.01,-.01,'0011223344',],
    [10000002,-10000002,datetime.date(2000,8,4),datetime.date(2000,8,4),datetime.date(2000,8,4),datetime.date(2000,8,4),datetime.date(2000,8,4),.001,-.001,'00001'],
    [34488499,-34488499,datetime.date(2000,8,11),datetime.date(2000,8,11),datetime.date(2000,8,11),datetime.date(2000,8,11),datetime.date(2000,8,11),.0001,-.0001,'00'],
    [58866869,-58866869,datetime.date(2000,8,18),datetime.date(2000,8,18),datetime.date(2000,8,18),datetime.date(2000,8,18),datetime.date(2000,8,18),.00001,-.00001,None,],
    [84499484,-84499484,datetime.date(2000,8,25),datetime.date(2000,8,25),datetime.date(2000,8,25),datetime.date(2000,8,25),datetime.date(2000,8,25),.3,-.3,None,],
    [99999998,-99999998,datetime.date(2000,9,1),datetime.date(2000,9,1),datetime.date(2000,9,1),datetime.date(2000,9,1),datetime.date(2000,9,1),.04,-.04,None,],
    [99999999,-99999999,datetime.date(2000,9,8),datetime.date(2000,9,8),datetime.date(2000,9,8),datetime.date(2000,9,8),datetime.date(2000,9,8),.006,-.006,None,],
    [100000000,-100000000,datetime.date(2000,9,15),datetime.date(2000,9,15),datetime.date(2000,9,15),datetime.date(2000,9,15),datetime.date(2000,9,15),.0008,-.0008,None,],
    [100000001,-100000001,datetime.date(2000,9,22),datetime.date(2000,9,22),datetime.date(2000,9,22),datetime.date(2000,9,22),datetime.date(2000,9,22),.00009,-.00009,None,],
    [100000002,-100000002,datetime.date(2000,9,29),datetime.date(2000,9,29),datetime.date(2000,9,29),datetime.date(2000,9,29),datetime.date(2000,9,29),.22,-.22,None,],
    [374599483,-374599483,datetime.date(2000,10,6),datetime.date(2000,10,6),datetime.date(2000,10,6),datetime.date(2000,10,6),datetime.date(2000,10,6),.033,-.033,None,],
    [595598587,-595598587,datetime.date(2000,10,13),datetime.date(2000,10,13),datetime.date(2000,10,13),datetime.date(2000,10,13),datetime.date(2000,10,13),.0044,-.0044,None,],
    [999999998,-999999998,datetime.date(2000,10,20),datetime.date(2000,10,20),datetime.date(2000,10,20),datetime.date(2000,10,20),datetime.date(2000,10,20),.00055,-.00055,None,],
    [999999999,-999999999,datetime.date(2000,10,27),datetime.date(2000,10,27),datetime.date(2000,10,27),datetime.date(2000,10,27),datetime.date(2000,10,27),.000077,-.000077,None,],
    [1000000000,-1000000000,datetime.date(2000,11,3),datetime.date(2000,11,3),datetime.date(2000,11,3),datetime.date(2000,11,3),datetime.date(2000,11,3),.784,-.784,None,],
    [1000000001,-1000000001,datetime.date(2000,11,10),datetime.date(2000,11,10),datetime.date(2000,11,10),datetime.date(2000,11,10),datetime.date(2000,11,10),.0472,-.0472,None,],
    [1000000002,-1000000002,datetime.date(2000,11,17),datetime.date(2000,11,17),datetime.date(2000,11,17),datetime.date(2000,11,17),datetime.date(2000,11,17),.00908,-.00908,None,],
    [3747493856,-3747493856,datetime.date(2000,11,24),datetime.date(2000,11,24),datetime.date(2000,11,24),datetime.date(2000,11,24),datetime.date(2000,11,24),.000371,-.000371,None,],
    [7869595843,-7869595843,datetime.date(2000,12,1),datetime.date(2000,12,1),datetime.date(2000,12,1),datetime.date(2000,12,1),datetime.date(2000,12,1),.0000321,-.0000321,None,],
    [9999999998,-9999999998,datetime.date(2000,12,8),datetime.date(2000,12,8),datetime.date(2000,12,8),datetime.date(2000,12,8),datetime.date(2000,12,8),.8234,-.8234,None,],
    [9999999999,-9999999999,datetime.date(2000,12,15),datetime.date(2000,12,15),datetime.date(2000,12,15),datetime.date(2000,12,15),datetime.date(2000,12,15),.01111,-.01111,None,],
    [10000000000,-10000000000,datetime.date(2000,12,22),datetime.date(2000,12,22),datetime.date(2000,12,22),datetime.date(2000,12,22),datetime.date(2000,12,22),.004741,-.004741,None,],
    [10000000001,-10000000001,datetime.date(2000,12,29),datetime.date(2000,12,29),datetime.date(2000,12,29),datetime.date(2000,12,29),datetime.date(2000,12,29),.0007521,-.0007521,None,],
    [27500485848,-27500485848,datetime.date(2001,1,5),datetime.date(2001,1,5),datetime.date(2001,1,5),datetime.date(2001,1,5),datetime.date(2001,1,5),.00008267,-.00008267,None,],
    [65598383845,-65598383845,datetime.date(2001,1,12),datetime.date(2001,1,12),datetime.date(2001,1,12),datetime.date(2001,1,12),datetime.date(2001,1,12),.99999,-.99999,None,],
    [99999999998,-99999999998,datetime.date(2001,1,19),datetime.date(2001,1,19),datetime.date(2001,1,19),datetime.date(2001,1,19),datetime.date(2001,1,19),.023845,-.023845,None,],
    [99999999999,-99999999999,datetime.date(2001,1,26),datetime.date(2001,1,26),datetime.date(2001,1,26),datetime.date(2001,1,26),datetime.date(2001,1,26),.0088245,-.0088245,None,],
    [100000000000,-100000000000,datetime.date(2001,2,2),datetime.date(2001,2,2),datetime.date(2001,2,2),datetime.date(2001,2,2),datetime.date(2001,2,2),.00028853,-.00028853,None,],
    [100000000001,-100000000001,datetime.date(2001,2,9),datetime.date(2001,2,9),datetime.date(2001,2,9),datetime.date(2001,2,9),datetime.date(2001,2,9),.000058319,-.000058319,None,],
]


def setup_module():
    pass



class TestInline():

    def setup_class(self):
        pass

    def teardown_class(self):
        pass

    def test_inline(self):
        matrix = matrixb.Matrix(INLINE, COLUMN_NAMES, column_normalizer=None,
            column_types={colname:datetime.date for colname in DATE_COLUMNS}
        )
        self._test_matrix(matrix)

    def test_xlsx_default(self):
        matrix = matrixb.Importer('t/input/type-verification.xlsx', column_normalizer=None,
            column_types={colname:datetime.date for colname in DATE_COLUMNS} # by default. xlsx will provide datetime.datetime objects
        ).get_matrix()
        self._test_matrix(matrix)

    '''
    def test_googlesheet_default(self):
        matrix = matrixb.Importer('1K2VzXF9uuxATwI5ZyhO4nBho_z6L5ISa0mRDivvaaig', column_normalizer=None,
            column_types={colname:datetime.date for colname in DATE_COLUMNS} # by default. xlsx will provide datetime.datetime objects
        ).get_matrix()
        self._test_matrix(matrix)
    '''
    
    """
    def test_xls_default(self):
        matrix = matrixb.Importer('t/input/type-verification.xls', column_normalizer=None,
            #column_types={colname:datetime.date for colname in DATE_COLUMNS} # by default. xlsx will provide datetime.datetime objects
        ).get_matrix()
        self._test_matrix(matrix)
    """

    def test_csv_default(self):
        matrix = matrixb.Importer('t/input/type-verification.csv', column_normalizer=None,
            column_types={colname:datetime.date for colname in DATE_COLUMNS}
        ).get_matrix()
        self._test_matrix(matrix)

    def test_csv_overquoted(self):
        matrix = matrixb.Importer('t/input/type-verification-overquote.csv', column_normalizer=None,
            column_types={colname:datetime.date for colname in DATE_COLUMNS}
        ).get_matrix()
        self._test_matrix(matrix)


    def _test_matrix(self, matrix):
        for irow in range(matrix.nrows):
            for icol in range(matrix.ncolumns):
                assert type(matrix[irow][icol]) == type(REFERENCE[irow][icol]), "Expected " + str(matrix[irow][icol]) +" "+ str(type(matrix[irow][icol]))+" to be type " + str(type(REFERENCE[irow][icol])) + "\n\tFull Row: " + str(matrix[irow]) 
                assert matrix[irow][icol] == REFERENCE[irow][icol], "Expected " + str(matrix[irow][icol]) + " to equal " + str(REFERENCE[irow][icol]) + "\n\tFull Row: " + str(matrix[irow])
#        assert_expectations()
        print("Completed test suite for matrix with source: ", end="")
        print(matrix.source)



def build_variants():

    build_csv_type_verification()
    build_xlsx_type_verification()

def build_csv_type_verification():
    import csv
    with open('t/input/type-verification-overquote.csv', 'w', newline="") as csvfile:
        writer = csv.writer(csvfile, delimiter=',', quotechar='"', quoting=csv.QUOTE_ALL)
        writer.writerow(COLUMN_NAMES)
        for row in INLINE:
            writer.writerow(row)

    with open('t/input/type-verification.csv', 'w', newline="") as csvfile:
        writer = csv.writer(csvfile, delimiter=',', quotechar='"')
        writer.writerow(COLUMN_NAMES)
        for row in INLINE:
            writer.writerow(row)

def build_xlsx_type_verification():
    import xlsxwriter
    wb = xlsxwriter.Workbook('t/input/type-verification.xlsx')

    COLUMN_TYPES = {
        '2 Digit Year': wb.add_format({'num_format': 'm/d/yy'}),
        '4 Digit Year': wb.add_format({'num_format': 'mm-dd-yyyy'}),
        'ISO Date': wb.add_format({'num_format': 'yyyy-mm-dd'}),
        'Full Month Date': wb.add_format({'num_format': 'mmmm dd, yyyy'}),
        'Abbreviated Date': wb.add_format({'num_format': 'mmm d, yyyy'}),
    }

    ws = wb.add_worksheet('Type Verification')
    ws.write_row(0, 0, COLUMN_NAMES)
    for irow in range(len(REFERENCE)):
        ws.write_row(irow+1, 0, REFERENCE[irow])
    for colname, fmt in COLUMN_TYPES.items():
        ws.set_column(COLUMN_NAMES.index(colname),COLUMN_NAMES.index(colname),20,fmt)
    wb.close()


if __name__ == "__main__":
    build_variants()
