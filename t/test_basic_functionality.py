import pytest
import os, sys, shutil
import datetime

sys.path.insert(0, '../datacleaner')
sys.path.append('.')
import matrixb

def setup_module():
    pass

class TestMatrixFunctionality():

    def verify_matrices(self, mb, expected_matrix, expected_columns):
        assert mb
        for i, val in enumerate(expected_columns):
            assert mb.columns[i] == val

        for irow, row in enumerate(expected_matrix):
            assert len(row) == mb.ncolumns
            for icol, col in enumerate(row):
                assert col == expected_matrix[irow][icol]

        assert len(mb) == mb.nrows
        assert len(mb) == len(expected_matrix)
        assert len(mb[0]) == len(expected_matrix[0])
        assert len(mb[0]) == mb.ncolumns


    def test_simplematrix(self):
        matrix = [[1,2,3,4,5], [6,7,8,9,10], [11,12,13,14,15]]
        mb = matrixb.Matrix(matrix, columns=['A','B','C','D','E'])
        self.verify_matrices(mb, matrix, ['a','b','c','d','e'])


    def test_multiple_iterators(self):
        matrix = [[1,2,3,4,5], [6,7,8,9,10], [11,12,13,14,15], [16,17,18,19,20]]
        mb = matrixb.matrix.Matrix(matrix, columns=['A','B','C','D','E'])
        self.verify_matrices(mb, matrix, ['a','b','c','d','e'])

        iter1 = iter(mb)
        test1_1 = next(iter1)
        test1_2 = next(iter1)

        iter2 = iter(mb)
        test2_1 = next(iter2)
        assert test1_1[0] == test2_1[0]
        assert test1_1[3] == test2_1[3]
        assert test1_2[0] != test2_1[0]
        assert test1_1[0] == mb[0][0]
        assert test2_1[0] == mb[0][0]

        test2_2 = next(iter2)
        assert test1_1[0] != test2_2[0]
        assert test1_1[3] != test2_2[3]
        assert test1_2[0] == test2_2[0]
        assert test2_1[2] == mb[0][2]
        assert test1_1[0] == mb[0][0]

    def test_array_functions(self):
        matrix = [[1,2,3,4,5], [6,7,8,9,10], [11,12,13,14,15]]
        columns = ['a','b','c','d','e']
        mb = matrixb.Matrix(matrix, columns)
        self.verify_matrices(mb, matrix, columns)

        add1 = ['t','e','s','t','x']

        mb.append(add1)
        self.verify_matrices(mb, matrix + [add1], columns)
        matrix += [ add1 ]

        ext = [
            [1.0, 2.2, 3.33, -1.5, -.5],
            [True, False, True, False, True],
        ]
        mb.extend(ext)
        self.verify_matrices(mb, matrix + ext, columns)
        matrix += ext

        # appending rows that are smaller than the number of columns should
        # auto-extend them to be the same length, to guarantee any iterator calls to
        # matrixb[i][ncol-1] does not throw an IndexError

        small1 = [1,2,3]
        mb.append(small1)
        assert len(mb[-1]) == mb.ncolumns
        for icol in range(3, mb.ncolumns):
            assert mb[-1][icol] == None

        small2 = [4,5,6]
        mb.extend( [ small2 ] )
        assert len(mb[-1]) == mb.ncolumns
        for icol in range(3, mb.ncolumns):
            assert mb[-1][icol] == None

        small1 += [None, None]
        small2 += [None, None]
        self.verify_matrices(mb, matrix + [small1, small2], columns)
        matrix += [small1, small2]

        with pytest.raises(TypeError):
            mb.append(1)

        with pytest.raises(Exception):
            mb.append([1,2,3,4,5,6])

        with pytest.raises(TypeError):
            mb.extend([1])

        with pytest.raises(Exception):
            mb.extend([ [1,2,3,4,5,6] ])

        # make sure there are no side effects from failed append/extend calls
        self.verify_matrices(mb, matrix, columns)


class TestTypeMatching():
    """ This class tests all the functionality around identifying and translating matrix data into
    correct types, but with autoclean functionality and by specifying column types explicitly """

    def test_formats(self):
        mb = matrixb.matrix.Matrix( [['', ' ', None],
                                     [' what', ' 1', '1.33 ']],
                                    columns=['A  ', ' B', 'C '])

        assert mb.columns[0] == 'a'
        assert mb.columns[1] == 'b'
        assert mb.columns[2] == 'c'
        assert len(mb) == 1

        assert mb[0][0] == 'what'
        assert mb[0][1] == 1
        assert mb[0][2] == 1.33

    def test_dates(self):

        data = [[1, '2017-01-03'], [2, '2/1/2018'], [3, '11/15/2019']]

        def validate(matrix):
            assert type(matrix[0][1]) is datetime.date
            assert matrix[0][1] == datetime.date(2017,1,3)
            assert matrix[1][1] == datetime.date(2018,2,1)
            assert matrix[2][1] == datetime.date(2019,11,15)


        mb = matrixb.Matrix(data, columns=['#', 'date'],
                            column_types=[int,datetime.date],
                            )
        validate(mb)


        mb = matrixb.Matrix(data, columns=['#', 'date'],
                            column_types={1:datetime.date},
                            )
        validate(mb)

        mb = matrixb.Matrix(data, columns=['#', 'date'],
                            column_types={'date':datetime.date},
                            )
        validate(mb)
