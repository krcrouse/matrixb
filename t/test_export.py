import pytest
import os, sys, shutil
import datetime
import pickle
import tempfile

sys.path.insert(0, '.')
import matrixb

export_sets = {
    'numeric_matrix': (
        [[1,2,3,4,5], [6,7,8,9,10], [11,12,13,14,15], [16,17,18,19,20]],
        ['A','B','C','D','E']
    ),
}

def make_export_closure(matrix_params):
    def closure(self):
        matrix = matrixb.Matrix(*matrix_params)
        self.run_tests(matrix)
    return(closure)

class BaseTest():
    tempfiles = []

    def teardown_class(cls):
        for file in cls.tempfiles:
            if os.path.exists(file):
                os.remove(file)

    @classmethod
    def register_tempfile(cls, filename):
        cls.tempfiles.append(filename)

    def run_tests(self, source_mb):
        print("-- running tests --")
        file = tempfile.NamedTemporaryFile(delete=False, suffix='.' + self.suffix)
        filename = file.name
        file.close()
        self.register_tempfile(filename)

        source_mb.export(filename)
        assert os.path.exists(filename), "export to filename - file exists"
        print("file", filename)
        restored = matrixb.Matrix(filename)

        for i, col in enumerate(source_mb.columns):
            assert source_mb.columns[i] == restored.columns[i], "Restored matrix column is the same as the source matrix column"

        for i, row in enumerate(source_mb):
            for j, val in enumerate(row):
                assert source_mb[i][j] == restored[i][j], f"Restored matrix value is correct for ({i},{j})"
        assert source_mb.nrows == restored.nrows, "Nrows match"

for key, params in export_sets.items():
    setattr(BaseTest, 'test_' + key, make_export_closure(params))

class TestCSV(BaseTest):

    def setup_class(cls):
        cls.suffix = 'csv'

class TestCSV2(BaseTest):

    def setup_class(cls):
        cls.suffix = 'CSV'

class TestCSV3(BaseTest):

    def setup_class(cls):
        cls.suffix = 'Csv'

class TestCSV4(BaseTest):

    def setup_class(cls):
        cls.suffix = 'csV'


class TestXLSX(BaseTest):

    def setup_class(cls):
        cls.suffix = 'xLSx'


class TestXLS(BaseTest):

    def setup_class(cls):
        cls.suffix = 'xls'

    def teardown_class(cls):
        super().teardown_class(cls)


#class TestODS(BaseTest):

#    def setup_class(cls):
#        cls.suffix = 'ods'
