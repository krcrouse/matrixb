import pytest
import os, sys, shutil
import datetime

sys.path.append('.')
import matrixb

def setup_module():
    pass 
    
class TestFactory():

    def setup_class(self):
        pass
    
    def teardown_class(self):
        pass 

    def reusable_load_test(self, filename):
        source = matrixb.source.factory(filename)
        assert source, "Factory method returns object for " + filename
        assert isinstance(source, matrixb.source.SourceBase), "Factory source is source instance"
        #assert source.header, "Header found"
        mb = source.get_matrix()
        assert mb, "get matrix returns object"
        assert isinstance(mb, matrixb.Matrix), "get_matrix returns matrix"
        assert type(mb.columns) is list, "resultant matrix has columns"
        assert len(mb.columns) == 7, "Resultant matrix has 4 columns"

    def test_load_csv(self):
        self.reusable_load_test('t/input/test.csv')

    def test_load_xlsx(self):
        self.reusable_load_test('t/input/test.xlsx')

    #def test_load_xls(self):
    #    self.reusable_load_test('t/input/test.xls')

    #def test_load_ods(self):
    #    self.reusable_load_test('t/input/test.ods')

