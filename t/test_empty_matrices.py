import pytest
import os, sys, shutil
import datetime

sys.path.insert(0, '.')
import matrixb

def setup_module():
    pass

class TestEmpty():

    def setup_class(self):
        pass

    def teardown_class(self):
        pass

    def test_empty(self):
        null = matrixb.Matrix()
        assert null is not None, "a null matrix can be created."

        assert null.nrows == 0, "nrow of an emptily-created matrix is 0"
        assert null.ncolumns is None, "ncolumns of an emptily-created matrix is None"
        null.append([1,2,3,4,5])
        assert null.nrows == 1, "Appending 1 row to null registers as having 1 row"
        assert null.ncolumns == 5, "null registers as having 5 columns"
        assert null[0][0] == 1
        assert null[0][4] == 5
        null.append(['a','b','c'])
        assert null[1][0] == 'a'
        assert null[1][4] == None
        assert null.nrows == 2, "Appending 1 row to null registers as having 2 rows"
        assert null.ncolumns == 5, "null registers as having 5 columns"
        with pytest.raises(IndexError):
            null.append([1,2,3,4,5,6])

        with pytest.raises(Exception):
            null.rowcount_policy = 'accomodate'
        null.rowcount_policy = 'accommodate'
        null.append([1,2,3,4,5,6])
        assert null.nrows == 3, "Appending 1 row to null registers as having 3 rows"
        assert null.ncolumns == 6, "null registers as having 6 columns now"

        null.rowcount_policy = 'warning'
        # warning only works for smaller-than-normal arrays
        with pytest.raises(IndexError):
            null.append([1,2,3,4,5,6,7])
        null.append([1,2,3,4])
        assert null.nrows == 4, "Appending 1 row to null registers as having 4 rows"
        assert null.ncolumns == 6, "null registers as having 6 columns, because warning policy chops off the extra ones"
        assert null[3][0] == 1, "Correct value"
        assert null[3][3] == 4, "Correct value"
        assert null[3][4] == None, "Expected value"
        assert null[3][5] == None, "Expected value"
        with pytest.raises(IndexError):
            print(null[3][6])
