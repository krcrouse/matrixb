import pytest
import os, sys, shutil
import datetime

sys.path.insert(0, '../datacleaner')
sys.path.insert(0,'.')
import matrixb

def setup_module():
    pass

def core_matrix():
    mb = matrixb.Matrix([
        ['COL A ','col b','col (c)','^ColD ','Col 3','','Col GGGGG'],
        ['A2','B2','C2','D2','E2','','X'],
        [None,None,None,None,None,None,],
        ['A4','B4',' ','','E4','']
    ])
    return(mb)

    mb = matrixb.Matrix([
        [1,2,3,4,5],
        [None,None,None,None,None],
        ['a','b','c','d','e'],
    ])
    return(mb)

def empty_matrix():
    return(matrixb.Matrix(None, ['a','b','c']))

def empty_matrix_2():
    return(matrixb.Matrix([], ['a','b','c']))

def empty_matrix_3():
    return(matrixb.Matrix([
        [None,None,None],
        [None,None,None],
        [None,None,None],
    ], columns=['a','b','c']))


import_matrices = {
    'csv': lambda: matrixb.Importer('t/input/test.csv'),
    'xlsx': lambda: matrixb.Importer('t/input/test.xlsx'),
    #'ods': lambda: matrixb.Importer('t/input/test.ods')
}

class Test_Loaded_RowsLoaded():

    def test_resident_loaded(self):
        core = core_matrix()
        assert not core.loaded, "Resident matrix is NOT loaded by default because it is not cleaned."
        print(core._matrix)
        
        self.source_loaded_tests(core)

    def test_empty_matrix_None(self):        
 
        empty = empty_matrix()
        assert empty.loaded == True, "Empty matrix is true"
        assert not empty.has_next(), "Empty does not have next"
        assert empty.rows_loaded == 0, "Empty has 0 rows"

    def test_empty_matrix_None_2(self):
        empty = empty_matrix()
        assert not empty.has_next(), "Empty does not have next"
        assert empty.rows_loaded == 0, "Empty has 0 rows"
        assert empty.loaded == True, "Empty matrix is true"

    def test_empty_matrix_empty_Array(self):
        empty = empty_matrix_2()
        assert not empty.has_next(), "Empty does not have next"
        assert empty.rows_loaded == 0, "Empty has 0 rows"
        assert empty.loaded == True, "Empty matrix is true"


    def test_empty_matrix_empty_Array_2(self):
        empty = empty_matrix_2()
        assert empty.rows_loaded == 0, "Empty has 0 rows"
        assert empty.loaded == True, "Empty matrix is true"
        assert not empty.has_next(), "Empty does not have next"


    def source_has_next_tests(self, matrix):
        print(matrix.source.filename)
        assert matrix.has_next(), "Newly initialized matrix has_next"

    def source_loaded_tests(self, matrix):
        print(matrix.source.filename)
        assert not matrix.loaded, " matrix is not loaded by default"
        assert 0 == matrix.rows_loaded, "rows loaded is 0"

        itr = iter(matrix)
        for i in range(2):
            assert not matrix.loaded, "Still not loaded after iteration " + str(i)
            assert matrix.has_next(), "has_next() is true"
            row = next(itr)
            print(row)
            assert matrix.rows_loaded == i + 1, "rows loaded incremented correctly"

        assert matrix.loaded, "matrix is now fully loaded"
        assert matrix.has_next() == False, "has_next is now false"
        with pytest.raises(StopIteration):
            next(itr)

    def nrows_loaded_tests(self, matrix):
        matrix.load_policy = 'manual'
        with pytest.raises(Exception):
            matrix.nrows

        lazycopy = matrix.copy()
        lazycopy.load_policy = 'lazy'
        # need to verify the count here....
        assert lazycopy.nrows 
        assert lazycopy.loaded

        # while we're at it, lets veirfy copy doesn't have any weirdness going on.
        with pytest.raises(Exception):
            matrix.nrows


def generate_imported_closure(matrix_generator, generic_test_function):
    def func(self):
        importer = matrix_generator()
        getattr(self, generic_test_function)(importer.get_matrix())
    return(func)

for key, l in import_matrices.items():
    setattr(Test_Loaded_RowsLoaded, 'test_' + key, generate_imported_closure(l, 'source_loaded_tests'))
    setattr(Test_Loaded_RowsLoaded, 'test_nrows_' + key, generate_imported_closure(l, 'nrows_loaded_tests'))
